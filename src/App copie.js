import React from 'react';
import logo from './logo.svg';
import photo from './photo.jpg';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import SearchIcon from '@material-ui/icons/Search';
import GridList from '@material-ui/core/GridList';
import InputBase from '@material-ui/core/InputBase';



import './App.css';
import { Container } from '@material-ui/core';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <AppBar position="static" text-align="center">
            <Toolbar id="title">
            <img id="App-logo" src={logo} alt="logo" href={App}></img>
            <Typography component="h2" variant="h5" color="inherit" align="center" noWrap id="title">
              Diab'Kids
            </Typography>
            <IconButton color="inherit"> <SearchIcon/> </IconButton>
            </Toolbar>
            <Toolbar>
              <Link variant="body-1" color="inherit" id="title">
                Jeux
              </Link>
              <Link variant="body-1" color="inherit" id="title">
                Informations
              </Link>
              <Link variant="body-1" color="inherit" id="title">
                Relationnel
              </Link>
              <Link variant="body-1" color="inherit" id="title">
                L'Equipe
              </Link>
            </Toolbar>
          </AppBar>
      </header>

      <body className="App-body">
        <Grid container direction="row" justify="space-evely" alignItems="stretch" spacing={3}>
          <Grid item xs={12} sm={6} md={6} id="App-col1">
            <Container>
              <img src={photo} alt="Diabetes image" width="500" height="auto"></img>
            </Container>
          </Grid>
          <Grid item xs={12} sm={6} md={6} id="App-col2">
            <h1>BIENVENUE</h1>
            <Paper>
              <Typography component="h4" color="initial" gutterBottom id="contexte">
                    Cette plateforme ludique éducative est destinée aux enfants de 6 à 9 ans atteints de Diabète de Type 1.
              </Typography>
              <br/>
              </Paper>
            <Button size="large" variant="contained" color="primary">Commencer</Button>
          </Grid>
        </Grid>
      </body>

      <footer className="App-footer">
        <p>
          Copyright Diab'kids - Tous droits réservés <br/>
          <a href="#" id="contact">Nous contacter !</a>
        </p>
      </footer>
    </div>
  );
}

export default App;
