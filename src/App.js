import React from 'react';
import ReactDOM from 'react-dom';
import photo from './photo.png';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import Link from '@material-ui/core/Link';


import './App.css';
import { Container, TableRow, TableCell } from '@material-ui/core';

function App() {

  return (
    <div className="App">
      <header className="App-header">
      </header>

      <body className="App-body">
        <Grid container direction="row" justify="space-evely" alignItems="stretch" spacing={3}>
          <Grid item xs={12} sm={5} md={5} id="App-col1">
            <Container>
              <img src={photo} alt="Diabetes image" width="400" height="auto" id="logo"></img>
            </Container>
          </Grid>
          <Grid item xs={12} sm={7} md={7} className="App-col2">
            <h2 id="welcome">Bienvenue !</h2>
            <Paper className="contexte">
              <Typography gutterBottom>
                    Cette plateforme ludique éducative est destinée aux enfants de 6 à 9 ans atteints de Diabète de Type 1.
                    A travers des jeux, informations, conseils, contacts et bien d'autres, l'enfant sera incollable 
                    sur sa maladie et réussira à la gérer dans n'importe quelle situation ! <br/>
                    Le diabète n'est pas un handicap, il faut apprendre à le vivre au quotidien !
                    <p id="team">- L'équipe Diab'Kids</p>
              </Typography>
            </Paper>
            <Table>
              <TableRow>
                <TableCell><p id="question">Comment t'appelles-tu ?</p></TableCell>
                <TableCell>
                  <TextField required label="Obligatoire" defaultValue="Écris ton prénom ici" margin="normal" variant="filled" id="name"/>
                </TableCell>
              </TableRow>
            </Table>
            <Button id="button" size="large" variant="contained"><Link href="./Menu.js">Commencer</Link></Button>
          </Grid>
        </Grid>
      </body>

      <footer className="App-footer">
        <p>
          &copy; Copyright Diab'kids - Tous droits réservés <br/>
        </p>
      </footer>
    </div>
  );
}

export default App;
